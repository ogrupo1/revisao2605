#!/bin/bash

suc=0
fal=0

ping -c1 $1 &>/dev/null 

if [ $? -eq 0 ]; then
	suc=$(($suc + 1))
else
	fal=$(($fal + 1))
fi

ping -c1 $2 &>/dev/null

if [ $? -eq 0 ]; then
	suc=$(($suc + 1))
else
	fal=$(($fal + 1))
fi

ping -c1 $3 &>/dev/null

if [ $? -eq 0 ]; then
	suc=$(($suc + 1))
else
	fal=$(($fal + 1))
fi

ping -c1 $4 &>/dev/null

if [ $? -eq 0 ]; then
	suc=$(($suc + 1))
else
	fal=$(($fal + 1))
fi

printf "\nSucessos = $suc\n"
printf "\nFalhas = $fal\n\n"
