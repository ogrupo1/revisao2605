#!/bin/bash

arq=$1

test -e $arq || echo "Arquivo inexistente!" && test -e $arq || exit

printf "Escolha uma das opções\n\n[1] Ler as 10 primeiras linhas\n[2] Ler as 10 últimas linhas\n[3] Contar o número de linhas\n\n"

read esc

echo 

if [ $esc -eq 1 ]; then
	head -n 10 $arq 2>/dev/null ; exit
elif [ $esc -eq 2 ]; then
	tail -n 10 $arq 2>/dev/null ; exit
elif [ $esc -eq 3 ]; then
	echo $(wc -l < $arq)
else
	echo "Opção Inválida!!"
fi
